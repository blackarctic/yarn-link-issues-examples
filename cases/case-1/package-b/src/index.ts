import { getState } from "package-a";

export const retrieve = () => {
  return getState<string>("some-state");
};
