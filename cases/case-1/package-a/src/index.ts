const state = {};

export const setState = (key: string, data: any): void => {
  state[key] = data;
};

export const getState = <T = any>(key: string): T | undefined => {
  return state[key];
};
