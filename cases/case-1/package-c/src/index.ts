import { setState } from "package-a";

export const save = (data: string) => {
  setState("some-state", data);
};
