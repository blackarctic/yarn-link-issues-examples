import React from "react";
import { retrieve } from "package-b";
import { save } from "package-c";
import logo from "./logo.svg";
import "./App.css";

function App() {
  React.useEffect(() => {
    save("hello");
    const result = retrieve();
    if (result !== "hello") {
      throw new Error("state is incorrect");
    }
  }, []);

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
