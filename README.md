# Yarn Link Issues Examples

Various examples of cases where `yarn link` (as opposed to `yalc add`) fails to behave like an actual package install.

Note that since none of these packages are actually published on `npm`, we use `yalc add` to simulate installs from the `npm` registry.

## Case 1: stateful peer dependencies

`package-a` holds the state. `package-b` and `package-c` both depend on `package-a` as a peer dependency.

In this case, we are simulating developing `package-b` and `package-c` with `app-a`. This works as expected when using `yalc` but fails when using `link`.

### How to run

`./scripts/case-1 yalc` or `./scripts/case-1 link`

## Case 2: packages that install dependencies for other packages

`package-d` specifies the versions of `react` we want to use in `app-b`. However, we also use `package-e` and `package-f`, both of which specify installs of different versions of `react`.

In this case, we are simulating developing `package-e` and `package-f` with `app-b`. This fails (as it should) when using `yalc` but works fine (as it shouldn't) when using `link`.

### How to run

`./scripts/case-2 yalc` or `./scripts/case-2 link`
